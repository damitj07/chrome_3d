/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Gulp Dependencies
var gulp = require('gulp');
var rename = require('gulp-rename');

// Build Dependencies
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');

// Style Dependencies
var less = require('gulp-less');
var prefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');

// Development Dependencies
var jshint = require('gulp-jshint');

// Test Dependencies
var mochaPhantomjs = require('gulp-mocha-phantomjs');

// Development Dependencies
var jshint = require('gulp-jshint');

gulp.task('default', function () {
    // place code for your default task here
});
gulp.task('lint-client', function () {
    return gulp.src('./client/**/*.js')
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
});
gulp.task('lint-test', function () {
    return gulp.src('./test/**/*.js')
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
});

gulp.task('browserify-client', [ ], function () {
    return gulp.src('client/app.js')
            .pipe(browserify())
            .pipe(rename('app.js'))
            .pipe(gulp.dest('public/build'))
            .pipe(gulp.dest('public/javascripts'));
});


gulp.task('browserify-test', ['lint-test'], function () {
    return gulp.src('test/client/index.js')
            .pipe(browserify())
            .pipe(rename('client-test.js'))
            .pipe(gulp.dest('build'));
});

gulp.task('test', ['lint-test', 'browserify-test'], function () {
    return gulp.src('test/client/index.html')
            .pipe(mochaPhantomjs());
});

gulp.task('watch', function () {
    gulp.watch('client/**/*.js', ['browserify-client']);
    gulp.watch('test/client/**/*.js', ['browserify-test']);
});

gulp.task('styles', function () {
    return gulp.src('client/less/index.less')
            .pipe(less())
            .pipe(prefix({cascade: true}))
            .pipe(rename('car-finder.css'))
            .pipe(gulp.dest('build'))
            .pipe(gulp.dest('public/stylesheets'));
});

gulp.task('minify', ['styles'], function () {
    return gulp.src('build/car-finder.css')
            .pipe(minifyCSS())
            .pipe(rename('car-finder.min.css'))
            .pipe(gulp.dest('public/stylesheets'));
});

gulp.task('uglify', ['browserify-client'], function () {
    return gulp.src('build/app.js')
            .pipe(uglify())
            .pipe(rename('chrome_3d.min.js'))
            .pipe(gulp.dest('public/javascripts'));
});

gulp.task('build', ['uglify', 'minify']);