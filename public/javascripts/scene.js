/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    var container, stats;
    var camera, scene, renderer;
    var group;
    var targetRotation = 0;
    var targetRotationOnMouseDown = 0;
    var mouseX = 0;
    var mouseXOnMouseDown = 0;
    var canvasWidth = $('#chrome_3d').width(), canvasHeight = $('#chrome_3d').height();
    var windowHalfX = canvasWidth / 2;
    var windowHalfY = canvasHeight / 2;

    function init(targetDomId) {

        container = document.getElementById(targetDomId);
        //
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(60, canvasWidth / canvasHeight, 1, 1000);
        camera.position.set(0, 170, 550);
        scene.add(camera);

        // ### LIGHT  ###
        var lightPoint = new THREE.PointLight(0xfcfcfc, 0.7);
        camera.add(lightPoint);

        var light;
        light = new THREE.DirectionalLight(0xffffff, 1);
        light.position.set(0, 250, 0);
        light.castShadow = true;
        light.shadowMapWidth = 2048;
        light.shadowMapHeight = 2048;
        light.shadowCameraFar = 1000;
        light.shadowDarkness = 0.8;
        scene.add(light);

        var ambient = new THREE.AmbientLight(0x404040); // soft white light
        scene.add(ambient);

        // ### Model Group  ###
        group = new THREE.Group();
        group.castShadow = true;
        scene.add(group);
        //
        var groundMaterial = new THREE.MeshPhongMaterial({color: 0xffffff});
        var plane = new THREE.Mesh(new THREE.PlaneGeometry(1000, 1000), groundMaterial);
        plane.rotation.x = -Math.PI / 2;
        plane.receiveShadow = true;
        scene.add(plane);
        //
        var loader = new THREE.TextureLoader();
        var textureLogo = loader.load("./textures/shiny_chrome.png");
        textureLogo.wrapS = THREE.RepeatWrapping;
        textureLogo.wrapT = THREE.RepeatWrapping;
        textureLogo.repeat.set(0.00199, 0.0031);//w,h

        var textureReflect = loader.load("./textures/Shine_light.png");
        textureReflect.wrapS = THREE.RepeatWrapping;
        textureReflect.wrapT = THREE.RepeatWrapping;
        textureReflect.repeat.set(0.002, 0.002);
        //
        var shinyMatFront = new THREE.MeshBasicMaterial({
            specular: 0x050505,
            shininess: 100,
            map: textureLogo
        });
        var shinyMatBack = new THREE.MeshPhongMaterial({
            specular: 0x050505,
            shininess: 120,
            transparent: true,
//            map: textureReflect,
            opacity: 0.8
        });
        var shinyMatSide = new THREE.MeshPhongMaterial({
            color: 0x656565,
            specular: 0x050505,
            shininess: 50,
            transparent: true,
            opacity: 0.8
        });
        var materials = [shinyMatBack, shinyMatSide, shinyMatFront];
        var mat_chrome = new THREE.MeshFaceMaterial(materials);

        // ### SHAPES - GEOMWTRIES ###
        function addShape(geometry, material, x, y, z, rx, ry, rz, s) {
            var mesh = new THREE.Mesh(geometry, material);

            for (var face in mesh.geometry.faces) {
                if (mesh.geometry.faces[ face ].normal.z === 1) {
                    mesh.geometry.faces[ face ].materialIndex = 2;
                }
            }
            mesh.castShadow = true;
            mesh.position.set(x, y, z);
            mesh.rotation.set(rx, ry, rz);

            var box = new THREE.Box3().setFromObject(mesh);
            box.center(mesh.position); // this re-sets the mesh position
            mesh.position.multiplyScalar(-1);

            group.add(mesh);
        }

        var roundedRectShape = new THREE.Shape();
        roundedRect(roundedRectShape, 0, 0, 500, 320, 5);
        var extrudeSettings = {amount: 2, bevelEnabled: false, material: 0, extrudeMaterial: 1};
        var roundedRect3d = roundedRectShape.extrude(extrudeSettings);

        addShape(roundedRect3d, mat_chrome, 0, -200, 0, 0, 80.11, 0, 1);

        // ### RENDERER CONFIG ###
        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setClearColor(0xffffff);
        renderer.shadowMapEnabled = true;
        renderer.shadowMapType = THREE.PCFSoftShadowMap;
        renderer.shadowMapSoft = true;

        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(canvasWidth, canvasHeight);
        container.appendChild(renderer.domElement);

        container.addEventListener('mousedown', onDocumentMouseDown, false);
        container.addEventListener('touchstart', onDocumentTouchStart, false);
        container.addEventListener('touchmove', onDocumentTouchMove, false);
        //
        window.addEventListener('resize', onWindowResize, false);
    }
    function onWindowResize() {
        windowHalfX = canvasWidth / 2;
        windowHalfY = canvasHeight / 2;
        camera.aspect = canvasWidth / canvasHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(canvasWidth, canvasHeight);
    }
//
    function onDocumentMouseDown(event) {
        event.preventDefault();
        container.addEventListener('mousemove', onDocumentMouseMove, false);
        container.addEventListener('mouseup', onDocumentMouseUp, false);
        container.addEventListener('mouseout', onDocumentMouseOut, false);
        mouseXOnMouseDown = event.clientX - windowHalfX;
        targetRotationOnMouseDown = targetRotation;
    }
    function onDocumentMouseMove(event) {
        mouseX = event.clientX - windowHalfX;
        targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.02;
    }
    function onDocumentMouseUp(event) {
        container.removeEventListener('mousemove', onDocumentMouseMove, false);
        container.removeEventListener('mouseup', onDocumentMouseUp, false);
        container.removeEventListener('mouseout', onDocumentMouseOut, false);
    }
    function onDocumentMouseOut(event) {
        container.removeEventListener('mousemove', onDocumentMouseMove, false);
        container.removeEventListener('mouseup', onDocumentMouseUp, false);
        container.removeEventListener('mouseout', onDocumentMouseOut, false);
    }
    function onDocumentTouchStart(event) {
        if (event.touches.length == 1) {
            event.preventDefault();
            mouseXOnMouseDown = event.touches[ 0 ].pageX - windowHalfX;
            targetRotationOnMouseDown = targetRotation;
        }
    }
    function onDocumentTouchMove(event) {
        if (event.touches.length == 1) {
            event.preventDefault();
            mouseX = event.touches[ 0 ].pageX - windowHalfX;
            targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.05;
        }
    }
//
    function animate() {
        requestAnimationFrame(animate);
        render();
    }
    function render() {
        group.rotation.y += (targetRotation - group.rotation.y) * 0.02;
        renderer.render(scene, camera);
    }

    function renderScene(targetDomId) {
        init(targetDomId);
        animate();
        //17.389
//        var refreshId = setInterval(function () {
//            targetRotation += 0.03;
//            if ((Math.round(targetRotation * 10) / 10) >= 7.40 && (Math.round(targetRotation * 10) / 10) <= 7.60) {
//                clearInterval(refreshId);
//            }
//        }, 10);
    }
    function subDivisonModify(geometry, subdivisons) {
        var subDivmodifier = new THREE.SubdivisionModifier(subdivisons);
        var smooth = geometry.clone();
        subDivmodifier.modify(smooth);
        return smooth;
    }

    function roundedRect(ctx, x, y, width, height, radius) {

        ctx.moveTo(x, y + radius);
        ctx.lineTo(x, y + height - radius);
        ctx.quadraticCurveTo(x, y + height, x + radius, y + height);
        ctx.lineTo(x + width - radius, y + height);
        ctx.quadraticCurveTo(x + width, y + height, x + width, y + height - radius);
        ctx.lineTo(x + width, y + radius);
        ctx.quadraticCurveTo(x + width, y, x + width - radius, y);
        ctx.lineTo(x + radius, y);
        ctx.quadraticCurveTo(x, y, x, y + radius);

    }
    function createLight(color) {
        var pointLight = new THREE.PointLight(color, 1, 30);
        pointLight.castShadow = true;
        pointLight.shadow.camera.near = 1;
        pointLight.shadow.camera.far = 30;
        // pointLight.shadowCameraVisible = true;
        pointLight.shadow.bias = 0.01;
        var geometry = new THREE.SphereGeometry(0.3, 12, 6);
        var material = new THREE.MeshBasicMaterial({color: color});
        var sphere = new THREE.Mesh(geometry, material);
        pointLight.add(sphere);
        return pointLight;
    }

    var lastScrollTop = 0;
    var oneTEE = true;
    $(window).scroll(function (event) {
        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
            if (oneTEE) {
                oneTEE = false;
                // downscroll code
                var refreshId = setInterval(function () {
                    targetRotation += 0.03;
                    if ((Math.round(targetRotation * 10) / 10) >= 7.40 && (Math.round(targetRotation * 10) / 10) <= 7.60) {
                        $(window).off('scroll');
                        clearInterval(refreshId);
                    }
                }, 10);
            }
        } else {
            // upscroll code
        }
        lastScrollTop = st;
    });

    renderScene('chrome_3d');
})();