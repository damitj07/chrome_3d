/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var express = require("express");
var app = express();
var path = require('path');

//APP CONFIGURATION 
app.use(express.static('public')); // use this as resource  directory
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//APP ROUTING URL => FUNCTIONS
app.get('/', function (req, res) {
    res.sendFile(__dirname + "/public/index.html");
});

//START THE SERVER
app.listen(process.env.PORT || 9090, "0.0.0.0", function () {
    console.log("Chrome_3D has started at http://localhost:9090");
});